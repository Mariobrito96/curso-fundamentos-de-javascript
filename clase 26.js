class persona {
    constructor (nombre, apellido, altura) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.altura = altura;
    }
    saludar (fncSaludar) { //si no le llegan los parametros, la funcion debe ejecutarse de toas formas
        console.log(`Hola me llamo ${this.nombre} ${this.apellido}`);
        if (fncSaludar) {
            fncSaludar(this.nombre, this.apellido, false);
        }
    }
    soyAlto () {
        return this.altura >= 1.80;
    }
}

class desarrollador extends persona {
    constructor (nombre, apellido, altura) {
        super(nombre, apellido, altura)
    }

    saludar(fncSaludar) {
        console.log(`Hola me llamo ${this.nombre} ${this.apellido} y soy desarrollador`);
        if (fncSaludar) {
            fncSaludar(this.nombre, this.apellido, true);
        }
    }
}

function responderSaludo (nombre, apellido,isDev) {
    console.log(`hola buenos dias ${nombre} ${apellido}`);
    if (isDev) {
        console.log('ah mira, no sabia que eras desarrollador');
    }
}
var mario = new persona('Mario', 'Brito', 1.83);
var renan = new persona('Renan', 'Montero', 1.75);
var wilberth = new desarrollador('Wilberth', 'Notario',1.93);

mario.saludar(responderSaludo);//si no le envio los parametros, la funcion debe ejecutarse de toas formas si el responder
renan.saludar(responderSaludo);
wilberth.saludar(responderSaludo);
