const API_URL ='https://swapi.dev/api/';
const PEOPLE_URL = 'people/:id';
const opts = {crossDomain:true}; 

function obtenerPersonaje (id) {

    return new Promise ((resolve, reject) => {
        const URL = `${API_URL}${PEOPLE_URL.replace(':id', id)}`;
        $ 
        .get(URL, opts, function (data) {
            resolve(data)
        })
        .fail( ()=> reject(id))
    })
    
}

function onError (id) {
    console.log(`sucedio un error al obtener el personaje ${id}`)
}

 async function obtenerPersonajes () {
    var ids = [1, 2, 3, 4, 5, 6, 7];
    var promesas = ids.map( (id) => obtenerPersonaje(id) )//cuando la fnc solo retorna un valor no lleva llaves ni el return
    try {
        var personajes = await Promise.all(promesas);
        console.log(personajes);
    }
    catch (id){
        onError(id);
    }

}

obtenerPersonajes();

// obtenerPersonaje(1)
// .then(function (personaje) {
//     console.log(`El personaje es ${personaje.name}`)
//     return obtenerPersonaje(2);
// })
// .then(function (personaje) {
//     console.log(`El personaje es ${personaje.name}`)
//     return obtenerPersonaje(3);
// })
// .then(function (personaje) {
//     console.log(`El personaje es ${personaje.name}`)
//     return obtenerPersonaje(4);
// })
// .then(function (personaje) {
//     console.log(`El personaje es ${personaje.name}`)
//     return obtenerPersonaje(5);
// })
// .then(function (personaje) {
//     console.log(`El personaje es ${personaje.name}`)
//     return obtenerPersonaje(6);
// })
// .then(function (personaje) {
//     console.log(`El personaje es ${personaje.name}`)
//     return obtenerPersonaje(7);
// })
// .then(function (personaje) {
//     console.log(`El personaje es ${personaje.name}`)
// })
// .catch(onError);//forma simplificada, solo pasas la referencia de la funcion
// // .catch(function (id) {
// //     onError(id);
// // })

