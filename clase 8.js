var brito = {
    nombre: 'Mario',
    apellido: 'Brito',
    edad: 24
};

var renan = {
    nombre:'Renan',
    apellido:'Montero',
    edad: 25
};
imprimirNombreMayusculas(brito);
imprimirNombreMayusculas(renan);
var britoMasViejo = cumpleaños(brito);


function imprimirNombreMayusculas (persona) //podemos pedirle que obtenga el atributo nombre del objeto que le enviemos
{
    var {nombre}= persona
    console.log(nombre.toUpperCase());
}

function cumpleaños (persona) 
{
    //De esta manera modifico los valores del objeto fuera de la funcion
    // persona.edad += 1;
    // console.log(persona.nombre + " tiene " + persona.edad + " años");
    // De esta manera mantengo los valores del objeto dentro de la funcion y fuera como valores aparte
    return {
        ...persona,
        edad: persona.edad + 1

    }
}