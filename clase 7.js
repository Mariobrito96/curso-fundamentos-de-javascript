var brito = {
    nombre: 'Mario',
    apellido: 'Brito',
    edad: 24
};

var renan = {
    nombre:'Renan',
    apellido:'Montero',
    edad: 25
};
imprimirNombreMayusculas(brito);
imprimirNombreMayusculas(renan);


function imprimirNombreMayusculas (persona) //podemos pedirle que obtenga el atributo nombre del objeto que le enviemos
{
    var {nombre}= persona
    console.log(nombre.toUpperCase());
}