var brito = {
    nombre: 'Mario',
    apellido: 'Brito',
    edad: 24
};

var renan = {
    nombre:'Renan',
    apellido:'Montero',
    edad: 25
};
imprimirNombreMayusculas(brito);
imprimirNombreMayusculas(renan);
imprimirNombreMayusculas({nombre:'pepito'});//se puede definir un objeto dentro de la llamada a la funcion

function imprimirNombreMayusculas ({ nombre }) //podemos pedirle que obtenga el atributo nombre del objeto que le enviemos
{
    console.log(nombre.toUpperCase());
}