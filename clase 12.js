var brito = {
    nombre: 'Mario',
    apellido: 'Brito',
    edad: 24,
    ingeniero: true,
    cocinero: false,
    cantante: false
};
var juan = {
    nombre: 'juan',
    apellido: 'Gonzalez',
    edad: 17
};

const MAYORIA_DE_EDAD = 18;


function imprimirProfesiones (persona){
    
    console.log(`${persona.nombre} es: `);
    if (persona.ingeniero)//si es true
    {
        console.log('ingeniero');
    }
    if (persona.cocinero) 
    {
        console.log('cocinero');
    }
    if (persona.cantante) 
    {
        console.log('cantante');
    }
}

// const esMayorDeEdad = function (persona)
// {
//     return persona.edad >= MAYORIA_DE_EDAD;
// }
//arrow functions, es lo mismo que lo de arriba en funcionamiento del programa 
const esMayorDeEdad = persona =>  persona.edad >= MAYORIA_DE_EDAD;

function imprimirSiEsMayorDeEdad(persona) {
    if (esMayorDeEdad (persona))
    {
        console.log(`${persona.nombre} es mayor de edad`);
    }
    else {
        console.log(`${persona.nombre} no es mayor de edad`);
    }
}

function permitirAcceso (persona) {
    if(!esMayorDeEdad (persona))
    {
        console.log('Acceso Denegado');
    }
}