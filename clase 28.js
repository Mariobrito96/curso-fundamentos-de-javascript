const API_URL ='https://swapi.dev/api/';
const PEOPLE_URL = 'people/:id';
const opts = {crossDomain:true};

const onResponse = function (personaje) { 
    console.log(`Hola, yo soy ${personaje.name}`);
 }


function obtenerPersonaje (id) {
    const URL = `${API_URL}${PEOPLE_URL.replace(':id', id)}`;
    $.get (URL, opts, onResponse)
}

obtenerPersonaje(1);
obtenerPersonaje(2);
obtenerPersonaje(83);