function heredaDe (prototipoHijo, prototipoPadre) {
    var fnc = function () {}
    fnc.prototype = prototipoPadre.prototype;
    prototipoHijo.prototype = new fnc
    prototipoHijo.prototype.constructor = prototipoHijo;
}

function persona (nombre, apellido, altura) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.altura = altura;
    // return this; //no es necesario ponerlo, en js esta implicito que se retorna el objeto que se crea
}
persona.prototype.saludar = function() {
    console.log(`Hola me llamo ${this.nombre} ${this.apellido}`);
}
persona.prototype.soyAlto = function() {
    return this.altura >= 1.80;
}

function desarrollador (nombre, apellido) {
    this.nombre = nombre;
    this.apellido = apellido;
}
heredaDe(desarrollador, persona);

desarrollador.prototype.saludar = function () {
    console.log(`Hola me llamo ${this.nombre} ${this.apellido} y soy desarrollador`)
}
// var mario = new persona('Mario', 'Brito', 1.83);
// var renan = new persona('Renan', 'Montero', 1.75);
// var wilberth = new persona('Wilberth', 'Notario',1.93);