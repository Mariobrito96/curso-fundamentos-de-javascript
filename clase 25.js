class persona {
    constructor (nombre, apellido, altura) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.altura = altura;
    }
    saludar () {
        console.log(`Hola me llamo ${this.nombre} ${this.apellido}`);
    }
    soyAlto () {
        return this.altura >= 1.80;
    }
}

class desarrollador extends persona {
    constructor (nombre, apellido, altura) {
        super(nombre, apellido, altura)
    }

    saludar() {
        console.log(`Hola me llamo ${this.nombre} ${this.apellido} y soy desarrollador`);
    }
}

// var mario = new persona('Mario', 'Brito', 1.83);
// var renan = new persona('Renan', 'Montero', 1.75);
// var wilberth = new persona('Wilberth', 'Notario',1.93);